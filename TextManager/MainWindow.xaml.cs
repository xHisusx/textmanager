﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;

namespace TextManager
{
	public partial class MainWindow : Window
	{
		private System.Windows.Controls.TextBox[] tbxArr;
		private System.Windows.Controls.ComboBox[] cbxArr;
		private const int uiGroupCount = 4;

		/*		Используем два разных экземпляра цепочек подгружаемых блоков для их смены внутри OnScrollChanged
		 *		в зависимости от того хотим ли мы прогрузить основную цепочку файла или цепочку фильтров.
		 *		Есть более емкие и правильные способы для реализации такого интерфейса.								*/

		private List<String> lines = new List<String>();
		private List<String> linesFilter = new List<String>();
		private LayoutBlocks lbs = new LayoutBlocks();
		private LayoutBlocks lbsFilter = new LayoutBlocks();
		private bool filterBlocks = false;


		private Filter fl = new Filter();
		private Timer timer = null;

		private int startIndex = 0;

		public MainWindow()
		{
			InitializeComponent();
			Loaded += new RoutedEventHandler(MainWindow_Loaded);

			tbxArr = new System.Windows.Controls.TextBox[4] { tbx1, tbx2, tbx3, tbx4 };
			cbxArr = new System.Windows.Controls.ComboBox[4] { cbx1, cbx2, cbx3, cbx4 };
			StartBtn.IsEnabled = false;
		}


		/*		Ищем scrollviewer у нашего viewport, для события OnScrollChanged									*/
		void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			ScrollViewer scrollViewer = FindDescendant<ScrollViewer>(Viewport);
			scrollViewer.ScrollChanged += new ScrollChangedEventHandler(OnScrollChanged);
		}

		public static T FindDescendant<T>(DependencyObject obj) where T : DependencyObject
		{
			if (obj == null) return default(T);
			int numberChildren = VisualTreeHelper.GetChildrenCount(obj);
			if (numberChildren == 0) return default(T);

			for (int i = 0; i < numberChildren; i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(obj, i);
				if (child is T)
				{
					return (T)(object)child;
				}
			}

			for (int i = 0; i < numberChildren; i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(obj, i);
				var potentialMatch = FindDescendant<T>(child);
				if (potentialMatch != default(T))
				{
					return potentialMatch;
				}
			}

			return default(T);
		}


		private void ReadAndDisplay(string fileName)
		{
			lines.Clear();
			lbs.ClearBlocks();
			ResetSettings();
			ReadFile(fileName);
			lbs.InitializeBlocks(startIndex, lines);
			startIndex++;
			Viewport.Text = lbs.DisplayBlocks(startIndex, lines);
			StartBtn.IsEnabled = true;
		}

		private void ReadFile(string fileName)
		{
			Encoding encoding = Encoding.GetEncoding(1251);
			using (StreamReader file = new StreamReader(fileName, encoding))
			{
				string line;
				while ((line = file.ReadLine()) != null)
					lines.Add(line);
				file.Close();
			}
		}

		private void ResetSettings()
		{
			startIndex = 0;
			Viewport.Clear();
			ScrollViewer sw = FindDescendant<ScrollViewer>(Viewport);
			sw.ScrollToHome();
		}
		private void OptionsBtn_Click(object sender, RoutedEventArgs e)
		{

			OpenFileDialog fileDialog = new OpenFileDialog();
			fileDialog.Filter = "Text Files|*.txt|Log Files|*.log";
			fileDialog.DefaultExt = ".txt";
			if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				ReadAndDisplay(fileDialog.FileName);
		}

		private LayoutBlocks SetActiveClass()
		{
			if (filterBlocks)
				return (lbsFilter);
			return (lbs);
		}


		private List<String> SetActiveList()
		{
			if (filterBlocks)
				return (linesFilter);
			return (lines);
		}

		/*		Для прогрузки следущих блоков цепочки необходимо соприкосновение с краями Scrollbar.
		 *		Из Scrollviewer Viewport'а не получилось вытянуть ScrollBar. По этой причине пришлось
		 *		внедрить таймер, чтобы фильтровать многократное срабатывание скролла в краях ScrollViewer'a.		*/
		private void OnScrollChanged(Object sender, ScrollChangedEventArgs e)
		{
			var scrollViewer = (ScrollViewer)sender;
			if (timer == null)
			{
				timer = new Timer()
				{
					Enabled = false,
					Interval = 5,
					Tag = scrollViewer.Tag
				};
				timer.Tick += (s, ea) =>
				{
					if (scrollViewer.Tag == timer.Tag)
					{
						timer.Stop();
						if (IsLoaded)
						{
							LayoutBlocks lbsActive = SetActiveClass();
							List<String> listActive = SetActiveList();
							Console.WriteLine(startIndex);
							switch (GetScrollThumbPosition(e))
							{
								case ScrollThumbPosition.ReachedTheBottom:
									{
										
										if (!lbsActive.IsLastBlock(startIndex, listActive.Count))
										{
											scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight / 3 + scrollViewer.ScrollableHeight / 6 - scrollViewer.ViewportHeight / 2);
											startIndex++;
											Viewport.Text = lbsActive.DisplayBlocks(startIndex, listActive);
										}
										timer.Stop();
									}
									break;
								case ScrollThumbPosition.ReachedTheTop:
									{
										if (!lbsActive.IsFirstBlock(startIndex))
										{
											scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight / 3 + scrollViewer.ScrollableHeight / 6 - scrollViewer.ViewportHeight / 2);
											startIndex--;
											Viewport.Text = lbsActive.DisplayBlocks(startIndex, listActive);
										}
										timer.Stop();
									}
									break;
							}
						}
						timer.Dispose();
						timer = null;
					}
					else
					{
						timer.Tag = scrollViewer.Tag;
					}
				};
				timer.Start();
			}
		}

		private ScrollThumbPosition GetScrollThumbPosition(ScrollChangedEventArgs e)
		{
			if (e.VerticalOffset == 0)
				return ScrollThumbPosition.ReachedTheTop;
			else if (e.ExtentHeight - e.ViewportHeight - e.VerticalOffset == 0)
				return ScrollThumbPosition.ReachedTheBottom;
			else if (e.VerticalOffset + e.ViewportHeight >= e.ExtentHeight / 2 + (e.ExtentHeight - ((ScrollViewer)e.OriginalSource).ScrollableHeight) / 2)
				return ScrollThumbPosition.AfterTheMid;
			else if (e.VerticalOffset + e.ViewportHeight <= e.ExtentHeight / 2 + (e.ExtentHeight - ((ScrollViewer)e.OriginalSource).ScrollableHeight) / 2)
				return ScrollThumbPosition.UntilTheMid;

			throw new Exception("Ошибка расчёта положения ползунка скрола");
		}


		private enum ScrollThumbPosition : byte
		{
			ReachedTheTop, UntilTheMid, AfterTheMid, ReachedTheBottom
		}

		private bool IsAllTextboxEmpty()
		{
			for (int i = 0; i < uiGroupCount; i++)
			{
				if (tbxArr[i].Text != "" && tbxArr[i].Text != null)
					return (false);
			}
			return (true);
		}
		private void TryAddFilter(string content, List<String> filterList)
		{
			if (content != "")
				filterList.Add(content);
		}

		private void getFilterLists()
		{
			for (int i = 0; i < uiGroupCount; i++)
			{
				if (cbxArr[i].SelectedIndex == 0)
					TryAddFilter(tbxArr[i].Text, fl.andList);
				else if (cbxArr[i].SelectedIndex == 1)
					TryAddFilter(tbxArr[i].Text, fl.orList);
			}
		}

		private void StartBtn_Click(object sender, RoutedEventArgs e)
		{
			LayoutBlocks lbsActive = SetActiveClass();
			List<String> listActive = SetActiveList();

			if (IsAllTextboxEmpty())
			{
				filterBlocks = false;
				ResetSettings();
				startIndex++;
				Viewport.Text = lbs.DisplayBlocks(startIndex, lines);
			}
			else
			{
				filterBlocks = true;
				getFilterLists();
				linesFilter = fl.FilterWithContent(lines);
				ResetSettings();
				lbsFilter.ClearBlocks();
				lbsFilter.InitializeBlocks(startIndex, linesFilter);
				startIndex++;
				Viewport.Text = lbsFilter.DisplayBlocks(startIndex, linesFilter);
			}
		}

		private void Window_PreviewDrop(object sender, System.Windows.DragEventArgs e)
		{
			string fileName;

			if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
			{
				string[] files = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
				fileName = files[0].ToString();
				if (Directory.Exists(fileName))
				{
					System.Windows.MessageBox.Show("Выберите файл", "Ошибка");
					return;
				}
				ReadAndDisplay(fileName);
			}
		}

		private void Window_PreviewDragOver(object sender, System.Windows.DragEventArgs e)
		{
			e.Effects = System.Windows.DragDropEffects.All;
			e.Handled = true;
		}
	}
}
