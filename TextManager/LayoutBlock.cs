﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextManager
{
	class LayoutBlock
	{
		public int start { get; set; }
		public string text { get; set; }
	}
}
