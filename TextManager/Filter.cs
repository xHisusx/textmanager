﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextManager
{
	class Filter
	{

		public List<string> andList = new List<string>();
		public List<string> orList = new List<string>();

		public List<string> FilterWithContent(List<string> lines)
		{
			List<string> resList = new List<string>();
			foreach (string line in lines)
			{
				if (AndCompare(line, andList) && OrCompare(line, orList))
					resList.Add(line);
			}
			return (resList);
		}

		private bool OrCompare(string line, List<String> filterList)
		{
			if (filterList.Count == 0)
				return (true);
			foreach (string filter in filterList)
			{
				if (line.Contains(filter))
					return (true);
			}
			return (false);
		}

		private bool AndCompare(string line, List<String> filterList)
		{
			foreach (string filter in filterList)
			{
				if (!line.Contains(filter))
					return (false);
			}
			return (true);
		}

		public void clearFilters()
		{
			andList.Clear();
			orList.Clear();
		}
	}
}
