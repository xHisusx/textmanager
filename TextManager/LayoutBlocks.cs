﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace TextManager
{
	class LayoutBlocks
	{
		private List<LayoutBlock> layoutBlocks = new List<LayoutBlock>();
		private int blockLines = 250;

		public LayoutBlock TryCreateBlock(int startIndex, List<string> lines)
		{
			startIndex *= blockLines;

			LayoutBlock lb = layoutBlocks.Find(block => block.start == startIndex); 
			if (lb == null)
			{
				lb = CreateBlock(startIndex, lines);
				AddBlock(lb);
			}
			return (lb);
		}
		private LayoutBlock CreateBlock(int startIndex, List<string> lines)
		{
			int delta = 0;

			if (lines.Count == 0)
				return null;
			if (startIndex < 0)
				return null;
			if (startIndex + blockLines > lines.Count)
				delta = startIndex + blockLines - lines.Count;

			LayoutBlock lb = new LayoutBlock();
			lb.start = startIndex;
			for (int i = startIndex; i < startIndex + blockLines - delta; i++)
				lb.text += lines[i] + Environment.NewLine;
			return (lb);
		}
		public void AddBlock(LayoutBlock lb)
		{
			layoutBlocks.Add(lb);
		}
		public void AddBlock(int startIndex, string content)
		{
			startIndex *= blockLines;
			layoutBlocks.Add(new LayoutBlock() {start = startIndex, text = content});
		}

		public bool IsFirstBlock(int startIndex)
		{
			startIndex *= 250;
			if (startIndex - blockLines == 0)
				return (true);
			return (false);
		}
		public bool IsLastBlock(int startIndex, int linesCount)
		{
			startIndex *= 250;
			if (startIndex + blockLines > linesCount)
				return (true);
			return (false);
		}
		public string getResultTextById(int startIndex, int endIndex)
		{
			string res = "";
			startIndex *= blockLines;
			endIndex *= blockLines;
			var list = layoutBlocks.Where(block => block.start >= startIndex && block.start <= endIndex);
			foreach (LayoutBlock block in list)
				res += block.text;
			return (res);
		}
		public void InitializeBlocks(int startIndex, List<String> lines)
		{
			TryCreateBlock(startIndex, lines);
			startIndex++;
			TryCreateBlock(startIndex, lines);
		}
		public string DisplayBlocks(int startIndex, List<String> lines)
		{
			TryCreateBlock(startIndex, lines);
			return (getResultTextById(startIndex - 1, startIndex));
		}

		public void ClearBlocks()
		{
			layoutBlocks.Clear();
		}
	}
}
